Doom, with Buildstream
======================
This project demonstrates using our BSPs to package a graphical application.
In particular, that application is chocolate doom, and that BSP targets the Raspberry Pi 3B+.

Requirements
============

- A Raspberry PI 3B+
- An 8GB+ MicroSD card

Additionally, if building yourself;
- [buildstream 1.93.4](https://buildstream.build/install.html)
- An arm system with lots of ram. 8GB recommended.
    - swap can be used in a pinch but is not recommended.

Build
=====
Run this command on your most powerful arm device, or download an artifact from CI.
```
bst build deploy/image.bst
bst artifact checkout deploy/image.bst --directory raspi-img
```

Flash
=====
Connect an SD card to your system. The following assumes the SD card is at /dev/mmcblk0
```
tar xzf raspi-img/sdcard.tar.gz
sudo dd if=sdcard.img of=/dev/mmcblk0 bs=4k status=progress
sync
```
The sync is for good measure.  
Remove the SD card and connect it to your PI 3B+.

Run Doom
========
You will need a keyboard and mouse connected to the PI.

As of right now, weston won't launch on boot.  
Login as root and launch weston:
```
username: root
password: root
bash-5.0# weston
```

Once in weston, launch a terminal and execute chocolate-doom.
```
chocolate-doom -iwad /usr/games/id/Doom1.WAD
```

You can then use your keyboard/mouse to play doom.
